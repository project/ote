<?php
/**
 * @file
 * Default theme implementation to ote taglabel form.
 */
?>

<?php print drupal_get_css(); ?>
<?php print drupal_get_js(); ?>
<link rel="stylesheet" type="text/css" media="all" href="<?php print base_path().$lib['css']; ?>"/>
<?php print theme('status_messages'); ?>
<?php print $lib['content']; ?>
<script type="text/javascript" src="<?php print base_path().$variables['lib']['js']; ?>"></script>


 
