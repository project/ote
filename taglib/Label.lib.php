<?php
/**
 Tag Name: Label调用标签
 Tag URI: https://drupalhunter.com/taglib/label
 Author: Drupal Hunter
 Author URI: https://drupalhunter.com/
 Description: get the page vars by arg.
 */
 
function lib_label(&$ctag, $dpvar){
  $revalue = '';

  $id = $ctag->GetAtt('id');

  switch ($id) {
  case 'head':
    $revalue = $dpvar['head'];
    break;
  case 'head_title':
    $revalue = $dpvar['head_title'];
    break;
  case 'styles':
    $revalue = $dpvar['styles'];
    break;
  case 'scripts':
    $revalue = $dpvar['scripts'];
    break;
  case 'classes':
    $revalue = $dpvar['classes'];
    break;
  case 'page_top':
    $revalue = $dpvar['page_top'];
    break;
  case 'page':
    $revalue = $dpvar['page'];
    break;
  case 'page_bottom':
    $revalue = $dpvar['page_bottom'];
    break;
  default:
    $revalue = $dpvar['head'];
  }

  return $revalue;
}

function lib_label_form(){
  $form['views_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Views Name'),
    '#name' => 'views_name',
    '#default_value' => '',
    '#maxlength' => 255,
  );

  $form['views_display'] = array(
    '#type' => 'textfield',
    '#title' => t('Views Display'),
    '#name' => 'views_display',
    '#default_value' => '',
    '#maxlength' => 255,
  );

  $form['views_pager'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Views Pager'),
    '#name' => 'views_pager',
    '#default_value' => FALSE,
  );

  $form['views_items'] = array(
    '#type' => 'textfield',
    '#title' => t('Views Display Items'),
    '#name' => 'views_items',
    '#default_value' => '',
    '#maxlength' => 255,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Confirm"),
  );    

  return $form;
}
