<?php
/**
 Tag Name: Views调用标签
 Tag URI: https://drupalhunter.com/taglib/views
 Author: Drupal Hunter
 Author URI: https://drupalhunter.com/
 Description: get the views by arg.
 */

/**
 * lib views return value.
 */ 
function lib_views(&$ctag){
  $revalue = '';
  
  $innerText = $ctag->GetInnerText();

  $dtp = new TagParse();
  $dtp->SetNameSpace('field','[',']');
  $dtp->LoadSource($innerText);

  $views_name = $ctag->GetAtt('name');
  $views_display = $ctag->GetAtt('display');

  $output = array();
  if (isset($views_name)) {
    if (module_exists('views')) {
      if ($view = views_get_view($views_name)) {
        if ($view->access($views_display)) {
          $view->set_display($views_display);
          $view_output = $view->preview();
          if (!empty($view->result) || $view->display_handler->get_option('empty') || !empty($view->style_plugin->definition['even empty'])) {
            $output['#markup'] = $view_output;
          }
        }
        $view->destroy();
      }
    }
  }

  return drupal_render($output);
}

/**
 * lib views setting form.
 */
function lib_views_form($form, &$form_state){
  $form = array();
  $views = ote_get_views();
  $views_keys = array_keys($views);
  $selected_view = isset($views_keys[0]) ? $views_keys[0] : '';

  $form['#id'] = 'tagform';
  $form['views_name'] = array(
    '#title' => t('Select a view'),
    '#type' => 'select',
    '#options' => $views,
    '#ajax' => array(
      'callback' => '_ote_replace_view_displays_callback',
    ),
  );

  $form['views_display'] = array(
    '#type' => 'select',
    '#title' => 'display',
    '#options' => _ote_get_views_displays($selected_view),
    '#prefix' => '<div id="view-display-dropdown">',
    '#suffix' => '</div>',
  );

  $form['views_args'] = array(
    '#type' => 'textfield',
    '#title' => 'arguments',
    '#size' => '40',
    '#required' => FALSE,
    '#default_value' => '',
    '#description' => t('Additional arguments to send to the view as if they were part of the URL in the form of arg1/arg2/arg3. You may use %0, %1, ..., %N to grab arguments from the URL.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Confirm"),
  );    

  return $form;
}
