(function($){
/**
 * open layer for ote tag link.
 */
  var index = parent.layer.getFrameIndex(window.name); //Get the window index

  //Pass values to parent page
  $('.form-submit').on('click', function(){ 
      var tagform = document.getElementById("tagform");
      var views_name_index = tagform.views_name.selectedIndex;
      var views_display_index = tagform.views_display.selectedIndex;

      reval = '{HT:Views name="'+tagform.views_name.options[views_name_index].value+'" display="'+tagform.views_display.options[views_display_index].value+'" /}';
    
      parent.jQuery('#edit-file-content').insertAtCaret(reval);
      parent.layer.msg('Wow ~ actually succeeded ~', {icon: 6, time: 2000});
      parent.layer.close(index);
  });

})(jQuery);
