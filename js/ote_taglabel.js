(function($){
/**
 * open layer for ote tag link.
 */
Drupal.behaviors.ote = {
  attach: function(context, settings) {
    var taglist = $('.layer-taglist', context);    
    taglist.each(function () {
      $('#'+this.id).on('click', function(){
        layer.open({
          type: 2,
          area: ['650px', '440px'],
          fix: false,
          maxmin: true,
          content: '/drupal-7.34/admin/structure/ote/label/'+this.id,
        });
      });
    });    
  }
}

$.fn.extend({
  insertAtCaret: function(myValue) {
    var $t = $(this)[0];
    if (document.selection) {
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    } else if ($t.selectionStart || $t.selectionStart == '0') {
      var startPos = $t.selectionStart;
      var endPos = $t.selectionEnd;
      var scrollTop = $t.scrollTop;
      $t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos, $t.value.length);
      this.focus();
      $t.selectionStart = startPos + myValue.length;
      $t.selectionEnd = startPos + myValue.length;
      $t.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  }
})

})(jQuery);